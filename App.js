import React, {Component}       from "react";
import {
    StackNavigator,
    addNavigationHelpers}       from "react-navigation";
import { Provider, connect }    from "react-redux";
import {View}                   from "react-native";
import PropTypes                from "prop-types";

import * as notification        from "actions/notification";
import ButtonMenu               from "components/Menu/ButtonMenu";
import label                    from "language";
import Catalog                  from "components/Catalog/Catalog";
import WebVw                    from "components/WebVw";
import Login                    from "components/login/Login";
import Home                     from "components/Home";
import Notification             from "components/notification/Notification";
import LanguageSettings         from "components/Settings/language";
import getStore                 from "lib/store";
import PushController           from "lib/notification/PushController";



const RootNavigator = StackNavigator({
    Home: {
        screen: Home,
        navigationOptions: ({navigation}) => ({
            title: label.home,
            headerRight: <ButtonMenu />,
            headerLeft: null
        }),
    },
    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => ({
            title: label.auth,
            headerRight: <ButtonMenu />,
            headerLeft: null
        }),
    },
    Catalog: {
        screen: Catalog,
        navigationOptions: ({navigation}) => ({
            title:label.catalog,
            headerRight: <ButtonMenu />,
            headerLeft: null
        }),
    },
    WebVw: {
        screen: WebVw,
        navigationOptions: ({navigation}) => ({
            title:label.webvw,
            headerRight: <ButtonMenu />
        }),
    },
    Notification: {
        screen: Notification,
        navigationOptions: ({navigation}) => ({
            title: label.notification,
            headerRight: <ButtonMenu />,
            headerLeft: null
        }),
    },
    LanguageSettings: {
        screen: LanguageSettings,
        navigationOptions: ({navigation}) => ({
            title: label.selected_language,
            headerRight: <ButtonMenu />,
            headerLeft: null
        }),
    }
});

const stateReducer = (state, action) => {
    const newState = RootNavigator.router.getStateForAction(action, state);
    return newState || state;
};

@connect(store => ({
    state: store.state
}))
class AppWithNavigationState extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        state: PropTypes.object
    };
    render() {
        return (
            <RootNavigator
                navigation={addNavigationHelpers({
                    dispatch: this.props.dispatch,
                    state: this.props.state
                })}
            />
        );
    }
}
const store = getStore(stateReducer);


export default function NCAP() {
    return (
        <View style={{flex: 1}}>
            <Provider store={store}>
                <AppWithNavigationState />
            </Provider>
            <PushController
                dispatch={ store.dispatch }
                onChangeToken={(token)=>store.dispatch(notification.setToken(token))}
            />
        </View>
    );
}