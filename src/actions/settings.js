export function setLanguage(languageName) {
    return {
        type: "SET_LANGUAGE",
        data: languageName
    };
}