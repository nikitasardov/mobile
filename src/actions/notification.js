export function setToken(token) {
    return {
        type: "SET_TOKEN",
        data: token
    };
}

export function changeMainUrl(url) {
    return {
        type: "CHANGE_MAIN_URL",
        data: url
    };
}