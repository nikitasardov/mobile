export function add(num) {
    return {
        type: "ADD_TO_LIST",
        data: num
    };
}