import request from "react-native-axios";


export const getAll = () => dispatch => {
    request.get("https://jsonplaceholder.typicode.com/posts")
        .then(response => {
            dispatch({
                type: "GET_ALL_POSTS",
                data: response.data
            });
        });
};