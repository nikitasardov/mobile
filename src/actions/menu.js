export function toggle_menu(display) {
    return {
        type: "TOGGLE_MENU",
        data: display
    };
}