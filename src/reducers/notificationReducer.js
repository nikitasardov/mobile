const initialState = {
    token: "",
    mainUrl: "http://wed-planet.eu/"
};

export default function notification(state = initialState, action) {
    // let res=state;
    switch (action.type) {
    case "SET_TOKEN":
        return {
            ...state, token: action.data
        };
    case "CHANGE_MAIN_URL":
        return {
            ...state, mainUrl: action.data
        };
    default:
        return state;
    }
}