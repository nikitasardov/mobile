const initialState = {
    language: undefined
};

export default function settings(state = initialState, action) {
    let result = state;
    switch (action.type) {
    case "SET_LANGUAGE":
        result.language = action.data;
        break;
    }
    return result;
}