import { combineReducers } from "redux";

import list from "./listReducer";
import notification from "./notificationReducer";
import posts from "./postsReducer";
import menu from "./menuReducer";
import settings from "./settingsReducer";


export default function getRootReducer(stateReducer) {
    return combineReducers({
        state: stateReducer,
        listReducer: list,
        notification: notification,
        posts: posts,
        menu: menu,
        settings: settings,
    });
}