const initialState = [];

export default function list(state = initialState, action) {
    switch (action.type) {
    case "ADD_TO_LIST":
        return [
            ...state,
            action.data
        ];
    default:
        return state;
    }
}