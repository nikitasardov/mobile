const initialState = [];

export default function posts(state = initialState, action){
    let result = [];
    switch (action.type) {
    case "GET_ALL_POSTS":
        result = action.data;
        break;
    default:
        result = state;
        break;
    }
    return result === undefined ? [] : result;
}