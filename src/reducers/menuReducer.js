const initialState = false;

export default function menu(state = initialState, action) {
    switch (action.type) {
    case "TOGGLE_MENU":
        return action.data;
    default:
        return state;
    }
}