import {createStore,applyMiddleware} from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "remote-redux-devtools";

import getRootReducer from "../reducers";

export default function getStore(stateReducer) {
    const composeEnhancers = composeWithDevTools({ shouldHotReload: false });
    const store = createStore(
        getRootReducer(stateReducer),
        composeEnhancers(
            applyMiddleware(thunk)
        )
    );
    return store;
}