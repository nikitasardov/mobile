import React, { Component } from "react";
import { Platform } from "react-native";
import PropTypes from "prop-types";
import FCM, {FCMEvent} from "react-native-fcm";
import {addNavigationHelpers} from "react-navigation";


import * as notification from "actions/notification";


export default class PushController extends Component {
  static propTypes = {
      onChangeToken: PropTypes.func,
      dispatch: PropTypes.func,
      navigate: PropTypes.func
  };

  async componentDidMount() {
      try {
          let result = await FCM.requestPermissions({badge: false, sound: true, alert: true});
          console.log("FCM result: ", result);
      } catch (e) {
          console.error(e);
      }

      FCM.getFCMToken().then(token => {
          this.props.onChangeToken(token);
          console.log("token: " , token );
          FCM.subscribeToTopic("clients");
      });

      if (Platform.OS === "ios") {
          FCM.getAPNSToken().then(token => {
              console.log("APNS TOKEN (getFCMToken)", token);
          });
      }

      FCM.getInitialNotification().then(notif => {
          console.log("INITIAL NOTIFICATION", notif);
      });

      this.notificationListener = FCM.on(FCMEvent.Notification, notif => {
          console.log("new notif: ", notif);

          let data;
          notif.data!==undefined&&(data=JSON.parse( notif.data ));

          if (notif.local_notification) {
              return;
          }
          if (notif.opened_from_tray) {
              return;
          }
          if( notif.fcm.action === "open_item" ) {
              console.log( "open_item !!!" );
              this.props.dispatch( notification.changeMainUrl( data.item_url ) );
              addNavigationHelpers({
                  dispatch: this.props.dispatch,
                  state: {}
              }).navigate("WebVw");
          }

          if(notif.from === "/topics/clients" && notif.data!==undefined){
              notif["data"] = JSON.parse(notif["data"]);
              FCM.scheduleLocalNotification({
                  id: notif["google.message_id"],
                  fire_date: new Date().getTime(),
                  vibrate: 500,
                  title: notif["data"].title,
                  body: notif["data"].body,
                  sub_text: notif["data"].sub_text,
                  priority: "high",
                  large_icon: notif["data"].large_icon,
                  show_in_foreground: true,
                  picture: notif["data"].picture
              });
          }

          // if (Platform.OS === "ios") {
          //   // optional
          //   // iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
          //   // This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
          //   // notif._notificationType is available for iOS platfrom
          //   switch (notif._notificationType) {
          //     case NotificationType.Remote:
          //       notif.finish(RemoteNotificationResult.NewData); // other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
          //       break;
          //     case NotificationType.NotificationResponse:
          //       notif.finish();
          //       break;
          //     case NotificationType.WillPresent:
          //       notif.finish(WillPresentNotificationResult.All); // other types available: WillPresentNotificationResult.None
          //       break;
          //   }
          // }

          this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, token => {
              console.log("TOKEN (refreshUnsubscribe)", token);
              this.props.onChangeToken(token);
          });

          // direct channel related methods are ios only
          // directly channel is truned off in iOS by default, this method enables it
          FCM.enableDirectChannel();
          this.channelConnectionListener = FCM.on(FCMEvent.DirectChannelConnectionChanged, (data) => {
              console.log("direct channel connected" + data);
          });
          setTimeout(function () {
              FCM.isDirectChannelEstablished().then(d => console.log(d));
          }, 1000);
      });
  }

  componentWillUnmount() {
      if(this.notificationListener !== undefined){
          this.notificationListener.remove();
      }
      if(this.refreshTokenListener !== undefined){
          this.refreshTokenListener.remove();
      }
  }

  render() {
      return null;
  }
}