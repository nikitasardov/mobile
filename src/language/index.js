import LocalizedStrings from "react-native-localization";
import russian from "./russian";
import english from "./english";

let label = new LocalizedStrings({
    ru: russian,
    en: english
});

export default label;