const russian = {
    catalog: "Каталог",
    auth: "Авторизация",
    home: "Домой",
    notification: "Уведомления",
    webvw: "Browser",
    hide_menu: "Меню",
    show_menu: "Меню",
    selected_language: "Выбор языка",
};
export default russian;