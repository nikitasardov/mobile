const english = {
    catalog: "catalog",
    auth: "auth",
    home: "home",
    notification: "notification",
    webvw: "Browser",
    hide_menu: "hide menu",
    show_menu: "show menu",
    selected_language: "selected language",
};
export default english;