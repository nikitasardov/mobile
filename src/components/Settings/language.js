import React, { Component } from "react";
import {View, Text} from "react-native";

import PageDefault from "components/Page/PageDefault";
import LanguageSelect from "components/Menu/LanguageSelect";



export default class LanguageSettings extends Component {

    render() {
        return (
            <PageDefault>
                <View>
                    <LanguageSelect />
                </View>
            </PageDefault>
        );
    }
}