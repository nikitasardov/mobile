import React, { Component } from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import FCM, { FCMEvent } from "react-native-fcm";
import {
    Text,
    TouchableOpacity,
    View,
    Clipboard} from "react-native";

import styles from "./notificationStyle";
import PageDefault from "components/Page/PageDefault";
//import PushController from "lib/notification/PushController";
import firebaseClient from "./FirebaseClient";

@connect(state => ({
    notification: state.notification
}))
export default class Notification extends Component {

  static propTypes = {
      navigation: PropTypes.object,
      notification: PropTypes.object
  };

  constructor(props) {
      super(props);

      this.state = {
          token: this.props.notification.token,
          tokenCopyFeedback: ""
      };
  }

  componentDidMount() {
      FCM.getInitialNotification().then(notif => {
          this.setState({
              initNotif: notif
          });
      });

      FCM.on(FCMEvent.RefreshToken, (token) => {
          console.log("RefreshToken: ", token);
      // fcm token may not be available on first load, catch it here
      });
  }

  showLocalNotification() {
      FCM.presentLocalNotification({
          vibrate: 500,
          title: "Hello",
          body: "Test Notification",
          big_text: "i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large, i am large",
          priority: "high",
          sound: "bell.mp3",
          large_icon: "https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg",
          show_in_foreground: true,
          number: 10
      });
  }

  scheduleLocalNotification() {
      FCM.scheduleLocalNotification({
          id: "testnotif",
          fire_date: new Date().getTime() + 5000,
          vibrate: 500,
          title: "Hello",
          body: "Test Scheduled Notification",
          sub_text: "sub text",
          priority: "high",
          large_icon: "https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg",
          show_in_foreground: true,
          picture: "https://firebase.google.com/_static/af7ae4b3fc/images/firebase/lockup.png"
      });
  }

  render() {
      let { token, tokenCopyFeedback } = this.state;

      return (
          <PageDefault>
              <View style={styles.container}>
                  {/*<PushController*/}
                  {/*onChangeToken={token => this.setState({token: token || ""})}*/}
                  {/*/>*/}
                  <Text style={styles.welcome}>
              Welcome to Simple Fcm Client!
                  </Text>

                  <Text>
              Token feedback: {this.state.tokenCopyFeedback}
                  </Text>

                  <Text selectable={true} onPress={() => this.setClipboardContent(this.state.token)} style={styles.instructions}>
              Token: {this.state.token}
                  </Text>

                  <Text style={styles.feedback}>
                      {this.state.tokenCopyFeedback}
                  </Text>

                  <Text style={styles.feedback}>
              Remote notif won{"\'"}t be available to iOS emulators
                  </Text>

                  <TouchableOpacity onPress={() => firebaseClient.sendNotification(token)} style={styles.button}>
                      <Text style={styles.buttonText}>Send Notification</Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => firebaseClient.sendData(token)} style={styles.button}>
                      <Text style={styles.buttonText}>Send Data</Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => firebaseClient.sendNotificationWithData(token)} style={styles.button}>
                      <Text style={styles.buttonText}>Send Notification With Data</Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.showLocalNotification()} style={styles.button}>
                      <Text style={styles.buttonText}>Send Local Notification</Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.scheduleLocalNotification()} style={styles.button}>
                      <Text style={styles.buttonText}>Schedule Notification in 5s</Text>
                  </TouchableOpacity>
              </View>
          </PageDefault>
      );
  }

  setClipboardContent(text) {
      Clipboard.setString(text);
      this.setState({tokenCopyFeedback: "Token copied to clipboard."});
      setTimeout(() => { this.clearTokenCopyFeedback(); }, 2000);
  }

  clearTokenCopyFeedback() {
      this.setState({tokenCopyFeedback: ""});
  }
}