import {Button} from "react-native";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import label from "language";
import * as menuAction from "actions/menu";

@connect(state => ({
    menu: state.menu
}))
export default class ButtonMenu extends Component {

    static propTypes = {
        setLanguage: PropTypes.func,
        dispatch: PropTypes.func,
        menu: PropTypes.bool
    };

    toggleMenu = () =>{
        this.props.dispatch(menuAction.toggle_menu(!this.props.menu));
    };

    render() {
        return (
            <Button
                title={this.props.menu === true ? label.hide_menu : label.show_menu}
                onPress={this.toggleMenu}
            />
        );
    }
}