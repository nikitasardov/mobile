import React, { Component }     from "react";
import PropTypes                from "prop-types";
import {addNavigationHelpers}   from "react-navigation";
import {connect}                from "react-redux";
import {
    View,
    Text,
    TouchableHighlight}         from "react-native";


import menuStyles               from "./menuStyles";
import label                    from "language";
import * as menuAction          from "../../actions/menu";

@connect(state => ({
    menu: state.menu
}))
export default class Menu extends Component<{}> {

    static propTypes = {
        navigate: PropTypes.func,
        dispatch: PropTypes.func,
        close: PropTypes.func
    };

    _navigation = addNavigationHelpers({
        dispatch: this.props.dispatch,
        state: {}
    });

    goTo = (name) => {
        this._navigation.navigate(name);
        this.props.dispatch(menuAction.toggle_menu(false));
    };

    render() {
        let buttons = [
            {title: label.home, link: "Home"},
            {title: label.auth, link: "Login"},
            {title: label.catalog, link: "Catalog"},
            {title: label.notification, link: "Notification"},
            {title: label.webvw, link: "WebVw"},
            {title: label.selected_language, link: "LanguageSettings"},
        ];
        return (
            <View style={{ flex: 1 }}>
                <View style={menuStyles.list}>
                    {buttons.map(item => (
                        <TouchableHighlight
                            underlayColor={"transparent"}
                            key={item.link}
                            style={menuStyles.item}
                            onPress={() => this.goTo(item.link)}
                        >
                            <Text>{item.title}</Text>
                        </TouchableHighlight>
                    ))}
                </View>
            </View>
        );
    }
};