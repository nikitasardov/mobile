import React, {Component}       from "react";
import PropTypes                from "prop-types";
import {addNavigationHelpers}   from "react-navigation";
import {connect}                from "react-redux";
import {Button,View}            from "react-native";

import label                    from "language";
import * as settingsAction      from "actions/settings";

@connect(state => ({
    settings: state.settings
}))
export default class LanguageSelect extends Component {

    static propTypes = {
        dispatch: PropTypes.func,
        settings: PropTypes.object
    };

    _navigation = addNavigationHelpers({
        dispatch: this.props.dispatch,
        state: {}
    });

    setLanguage = name => {
        label.setLanguage(name);
        this.props.dispatch(settingsAction.setLanguage(name));
        this._navigation.navigate("LanguageSettings");
    };

    render() {
        console.log("Settings: ", this.props.settings);
        return (
            <View>
                <Button
                    onPress={() => this.setLanguage("ru")}
                    title="RUS"
                />
                <Button
                    onPress={() => this.setLanguage("en")}
                    title="EN"
                />
            </View>
        );
    }
}