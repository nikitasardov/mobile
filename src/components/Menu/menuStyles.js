import {StyleSheet} from "react-native";
const menuStyles = StyleSheet.create({
    list: {
        padding: 10,
        flexDirection: "column",
        backgroundColor: "#FFF",
        height: "100%"
    },
    item: {
        margin: 3,
    }
});
export default menuStyles;