import React, {Component} from "react";
import {View,  ScrollView} from "react-native";
import Drawer from "react-native-drawer";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import Menu from "components/Menu";
import pageStyles from "./pageStyles";
import * as menuAction from "actions/menu";

@connect(state => ({
    notification: state.notification,
    menu: state.menu
}))
export default class PageDefault extends Component {

    static propTypes = {
        menu: PropTypes.bool,
        children: PropTypes.object,
        notification: PropTypes.object,
        dispatch: PropTypes.func
    };


    toggleMenu = display => {
        this.props.dispatch(menuAction.toggle_menu(display));
    };

    render() {
        return (
            <Drawer
                open={this.props.menu}
                type="overlay"
                ref={(ref) => this._drawer = ref}
                content={
                    <Menu />
                }
                style={pageStyles.drawer}
                tapToClose={true}
                panCloseMask={0.2}
                onClose={() => this.toggleMenu(this._drawer._open)}
                onOpen={() => this.toggleMenu(this._drawer._open)}
                openDrawerOffset={0.2}
                closedDrawerOffset={0}
                panOpenMask={0.2}
                tweenHandler={(ratio) => ({
                    main: { opacity: (2 - ratio) / 2 }
                })}
            >
                <ScrollView style={pageStyles.page}>
                    {this.props.children}
                </ScrollView>
            </Drawer>
        );
    }
}