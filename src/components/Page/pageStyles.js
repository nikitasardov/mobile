import {StyleSheet} from "react-native";
const pageStyles = StyleSheet.create({
    drawer: { shadowColor: "#000000", shadowOpacity: 0.8, shadowRadius: 3},
    page: {
        flex: 1,
        minWidth: "100%"
    }
});

export default pageStyles;