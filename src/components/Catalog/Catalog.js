import React, { Component } from "react";
import {View, Text} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import PageDefault from "components/Page/PageDefault";
import * as postsAction from "actions/posts";


@connect(state => ({
    posts: state.posts
}))
export default class Catalog extends Component {

  static propTypes = {
      navigation: PropTypes.object,
      dispatch: PropTypes.func,
      posts: PropTypes.array
  };

  componentDidMount(){
      this.props.dispatch(postsAction.getAll());
  };


  render() {
      const { posts } = this.props;
      return (
          <PageDefault>
              <View>
                  {posts.map(item=>
                      <Text key={item.id}>{item.id+") "+item.title}</Text>
                  )}
              </View>
          </PageDefault>
      );
  }
}