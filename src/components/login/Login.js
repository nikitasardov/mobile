import React, { Component } from "react";
import PropTypes from "prop-types";
import {
    Text,
    AsyncStorage,
    TextInput,
    Alert,
    View,
    Button
} from "react-native";

import PageDefault from "components/Page/PageDefault";
import styles from "./loginStyles";

export default class Login extends Component<{}> {

  static propTypes = {
      navigation: PropTypes.object
  };

  constructor(props) {
      super(props);

      this.state = {
          text: ""
      };

      AsyncStorage.getItem("text").then((value) => {
          this.setState({"text": value});
      });
  }

    alert = text => {
        Alert.alert(text);
    };

    saveData = () => {
        try {
            AsyncStorage.setItem("text", this.state.text, this.alert("saved"));
        } catch (error) {
            this.alert(error);
        }
    };

    render() {
        return (
            <PageDefault>
                <View style={styles.container}>
                    <Text>Text:{this.state.text}</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({text})}
                        value={this.state.text}
                    />

                    <Button
                        title="Save Form"
                        onPress={() =>
                            this.saveData()
                        }
                    />
                </View>
            </PageDefault>
        );
    }
}