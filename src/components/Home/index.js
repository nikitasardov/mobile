import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    Button,
    Text,
    View,
    WebView,
    AsyncStorage} from "react-native";

import PageDefault from "components/Page/PageDefault";
import * as listAction from "../../actions/list";

@connect(state => ({
    listReducer: state.listReducer,
    notification: state.notification
}))
export default class Home extends Component {

    static propTypes = {
        dispatch: PropTypes.func,
        navigation: PropTypes.object,
        notification: PropTypes.object,
        listReducer: PropTypes.array
    };

    constructor(props) {
        super(props);

        AsyncStorage.getItem("text").then((value) => {
            this.setState({"text": value});
        });

        this.state = {
            text: ""
        };
    }

    add = () => {
        this.props.dispatch(listAction.add(1));
    };

    render() {
        return (
            <PageDefault>
                <View>
                    <Text>Hello, this home page!</Text>
                </View>
            </PageDefault>
        );
    }
}