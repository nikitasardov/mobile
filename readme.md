> Установка
1. npm i
2. Нам нужен SDK
    
    2.1 Для Android
    ![alt text](./readme/sdk_for_android.jpg)

> Настройка Компилятора (Android)

![alt text](./readme/building_for_android.jpg)

> Решение проблем

если node server падает при react-native start
с ошибкой "res/drawable-hdpi ENOSPC"
решение тут

        https://stackoverflow.com/questions/22475849/node-js-error-enospc/32600959#32600959
        
> О полях в нотифе
    
        https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support